/*
 * Copyright (C) 2016 The Qt Company Ltd.
 * Copyright (C) 2016, 2017 Konsulko Group
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <QtCore/QDebug>
#include <QtCore/QCommandLineParser>
#include <QtCore/QUrlQuery>
#include <QtCore/QSettings>
#include <QtGui/QGuiApplication>
#include <QtQml/QQmlApplicationEngine>
#include <QtQml/QQmlContext>
#include <QtQuickControls2/QQuickStyle>
#include <QQuickWindow>
#include <libhomescreen.hpp>
#include <qlibwindowmanager.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <radio.h>
#include "PresetDataObject.h"

#define APP_DATA_PRESETS_PATH	"/app-data/radio/presets.conf"

int main(int argc, char *argv[])
{
    QString graphic_role = QString("radio");

    QGuiApplication app(argc, argv);

    QQuickStyle::setStyle("AGL");

    QCommandLineParser parser;
    parser.addPositionalArgument("port", app.translate("main", "port for binding"));
    parser.addPositionalArgument("secret", app.translate("main", "secret for binding"));
    parser.addHelpOption();
    parser.addVersionOption();
    parser.process(app);
    QStringList positionalArguments = parser.positionalArguments();

    // Read presets from configuration file
    //
    // If HOME is set, use $HOME/app-data/radio/presets.conf if it exists,
    // else fall back to the QSettings default locations with organization
    // "AGL" and a file name of radio-presets.conf. See:
    //
    // http://doc.qt.io/qt-5/qsettings.html#platform-specific-notes
    //
    // for details on the locations and their order of priority.
    //
    QSettings *pSettings = NULL;
    char *p = getenv("HOME");
    bool settingsFound = false;
    if(p) {
	struct stat statbuf;
        std::string presets(p);
	presets += APP_DATA_PRESETS_PATH;
	if(stat(presets.c_str(), &statbuf) == 0) {
	    QString confPath = p;
	    confPath.append(APP_DATA_PRESETS_PATH);
	    pSettings = new QSettings(confPath, QSettings::NativeFormat);
	    settingsFound = true;
	}
    }
    if(!settingsFound) {
	    pSettings = new QSettings("AGL", "radio-presets");
    }

    QList<QObject*> presetDataList;
    int size = pSettings->beginReadArray("fmPresets");
    for (int i = 0; i < size; ++i) {
        pSettings->setArrayIndex(i);
        presetDataList.append(new PresetDataObject(pSettings->value("title").toString(),
						   pSettings->value("frequency").toInt(),
						   1));
    }
    pSettings->endArray();

    QQmlApplicationEngine engine;
    QQmlContext *context = engine.rootContext();
    context->setContextProperty("presetModel", QVariant::fromValue(presetDataList));
    if (positionalArguments.length() == 2) {
        int port = positionalArguments.takeFirst().toInt();
        QString secret = positionalArguments.takeFirst();
        QUrl bindingAddress;
        bindingAddress.setScheme(QStringLiteral("ws"));
        bindingAddress.setHost(QStringLiteral("localhost"));
        bindingAddress.setPort(port);
        bindingAddress.setPath(QStringLiteral("/api"));
        QUrlQuery query;
        query.addQueryItem(QStringLiteral("token"), secret);
        bindingAddress.setQuery(query);
        context->setContextProperty(QStringLiteral("bindingAddress"), bindingAddress);

        std::string token = secret.toStdString();
        LibHomeScreen* hs = new LibHomeScreen();
        QLibWindowmanager* qwm = new QLibWindowmanager();

        // WindowManager
        if(qwm->init(port,secret) != 0){
            exit(EXIT_FAILURE);
        }
        AGLScreenInfo screenInfo(qwm->get_scale_factor());
        engine.rootContext()->setContextProperty(QStringLiteral("screenInfo"), &screenInfo);
        // Request a surface as described in layers.json windowmanager’s file
        if (qwm->requestSurface(graphic_role) != 0) {
            exit(EXIT_FAILURE);
        }
        // Create an event callback against an event type. Here a lambda is called when SyncDraw event occurs
        qwm->set_event_handler(QLibWindowmanager::Event_SyncDraw, [qwm, &graphic_role](json_object *object) {
            fprintf(stderr, "Surface got syncDraw!\n");
            qwm->endDraw(graphic_role);
        });

        // HomeScreen
        hs->init(port, token.c_str());
        // Set the event handler for Event_ShowWindow which will activate the surface for windowmanager
        hs->set_event_handler(LibHomeScreen::Event_ShowWindow, [qwm, &graphic_role](json_object *object){
            qDebug("Surface %s got showWindow\n", graphic_role.toStdString().c_str());
            qwm->activateWindow(graphic_role);
        });

        context->setContextProperty("radio", new Radio(bindingAddress, context));

        engine.load(QUrl(QStringLiteral("qrc:/Radio.qml")));
        QObject *root = engine.rootObjects().first();
        QQuickWindow *window = qobject_cast<QQuickWindow *>(root);
        QObject::connect(window, SIGNAL(frameSwapped()), qwm, SLOT(slotActivateWindow()
        ));
    }
    return app.exec();
}
